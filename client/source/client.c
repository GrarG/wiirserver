#include <stdio.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#define BUFFER_SIZE 1024

int main(int argc, char* argv[]){

	int sock, port;
	char buff[BUFFER_SIZE];
	struct sockaddr_in serveraddr;

	if(argc < 3){
		fprintf(stderr, "Retry with %s <IP> <PORT>\n", argv[0]);
		return -1;
	}

	port = atoi(argv[2]);

	if((sock=socket(AF_INET, SOCK_STREAM, IPPROTO_IP)) < 0){
		fprintf(stderr, "Failed to request system socket.\n");
		return -1;
	}

	bzero(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = inet_addr(argv[1]);
	serveraddr.sin_port = htons(port);

	if(connect(sock, (struct sockaddr*)&serveraddr, sizeof(serveraddr)) < 0){
		fprintf(stderr, "Unable to connect to %s:%d\n", argv[1], port);
		close(sock);
		return -1;
	}

	fprintf(stdout, "Connected to %s:%d\n. Comencing to read...\n", argv[1], port);

	while(1){
		read(sock, buff, BUFFER_SIZE);
		printf("Received: %s\n", buff);
	}

	close(sock);
}
