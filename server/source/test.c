#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <ogcsys.h>
#include <gccore.h>
#include <network.h>
#include <ogc/pad.h>
#include <ogc/es.h>
#include <ogc/machine/processor.h>
#include <ogc/ipc.h>
#include <ogc/wiilaunch.h>

#define SERVE_PORT 8034
#define SERVE_BUFFER_SIZE 1024

//---------------------------------------------------------------------------------
int main(int argc, char **argv) {
//---------------------------------------------------------------------------------

	u32 pressedPort;

	// Call WPAD_ScanPads each loop, this reads the latest controller states
	PAD_ScanPads();

	pressedPort = PAD_ButtonsDown(0);

	if(pressedPort & PAD_BUTTON_START){
		WII_LaunchTitle(0x100014C554C5ALL);
	}

	return 0;
}
