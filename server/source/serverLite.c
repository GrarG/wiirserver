#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <ogcsys.h>
#include <gccore.h>
#include <network.h>
#include <ogc/pad.h>
#include <ogc/es.h>
#include <ogc/machine/processor.h>
#include <ogc/ipc.h>
#include <ogc/wiilaunch.h>

#define SERVE_PORT 8034
#define SERVE_BUFFER_SIZE 1024

s32 launch_HBC();
void *initialize();
void *serve (void *arg);

static void *framebuffer = NULL;
static GXRModeObj *rmode = NULL;

static	lwp_t server_handle = (lwp_t)NULL;



//---------------------------------------------------------------------------------
int main(int argc, char **argv) {
//---------------------------------------------------------------------------------

	s32 ret;

	int i=0;
	char localip[16] = {0};
	char gateway[16] = {0};
	char netmask[16] = {0};
	u32 pressedPort[4];

	framebuffer = initialize();

	// The console understands VT terminal escape codes
	// This positions the cursor on row 2, column 0
	// we can use variables for this with format codes too
	// e.g. printf ("\x1b[%d;%dH", row, column );
	printf("\x1b[2;0H");

	printf("Configuring network ...\n");

	ret = if_config(localip, netmask, gateway, TRUE, 20);
	if(ret >= 0){
		printf("Network configured:\n"
			"\tIP:%s\n\tGATEWAY:\t%s\n\tMASK:\t%s\n", localip, gateway, netmask);
		LWP_CreateThread(&server_handle,	/* thread handle */
							serve,			/* code */
							localip,		/* arg pointer for thread */
							NULL,			/* stack base */
							16*1024,		/* stack size */
							50				/* thread priority */ );
	}else{
		printf("Could not configure the network. Are you online?\n");
	}

	printf("Network ready. Press Start on your GameCube controller to launch Homebrew Channel.\n");


	while(1) {

		// Call WPAD_ScanPads each loop, this reads the latest controller states
		PAD_ScanPads();

		// WPAD_ButtonsDown tells us which buttons were pressed in this loop
		// this is a "one shot" state which will not fire again until the button has been released
		for(i=0; i<4; i++){
			pressedPort[i] = PAD_ButtonsDown(i);
			if(pressedPort[i] & PAD_BUTTON_START){
				printf("Return code for HBC launch: %d\n", WII_LaunchTitle(0x100014C554C5ALL));
				//WII_LaunchTitle(0x100014C554C5ALL); // This title ID is LULZ, the Homebrew Channel's
			}
		}			

		// Wait for the next frame
		VIDEO_WaitVSync();
	}

	return 0;
}

//---------------------------------------------------------------------------------
void *initialize() {
//---------------------------------------------------------------------------------

	void *framebuffer;

	VIDEO_Init();
	PAD_Init();

	rmode = VIDEO_GetPreferredMode(NULL);
	framebuffer = MEM_K0_TO_K1(SYS_AllocateFramebuffer(rmode));
	console_init(framebuffer,20,20,rmode->fbWidth,rmode->xfbHeight,rmode->fbWidth*VI_DISPLAY_PIX_SZ);

	VIDEO_Configure(rmode);
	VIDEO_SetNextFramebuffer(framebuffer);
	VIDEO_SetBlack(FALSE);
	VIDEO_Flush();
	VIDEO_WaitVSync();
	if(rmode->viTVMode&VI_NON_INTERLACE) VIDEO_WaitVSync();

	return framebuffer;

}

void *serve (void *arg){

	int i = 0;
	int sock, csock;
	int ret;
	u32 clientlen;
	struct sockaddr_in client;
	struct sockaddr_in server;
	char buff[SERVE_BUFFER_SIZE];

	clientlen = sizeof(client);

	sock = net_socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
	if (sock == INVALID_SOCKET){
		printf("Couldn't create a socket.\n");
	}else{
		memset (&server, 0, sizeof (server));
		memset (&client, 0, sizeof (client));

		server.sin_family = AF_INET;
		server.sin_port = htons (SERVE_PORT);
		server.sin_addr.s_addr = INADDR_ANY;
		ret = net_bind(sock, (struct sockaddr *)&server, sizeof(server));

		if(ret){
			printf("Error %d when trying to bind socket to port %d.\n", ret, SERVE_PORT);
		}else{
			if((ret = net_listen(sock, 1))){
				printf("Error %d listening to socket.\n", ret);
			}else{
				while(1){
					csock = net_accept(sock, (struct sockaddr*)&client, &clientlen);
					if (csock < 0){
						printf("Couldn't accept incoming clients. (Error %d)\n", ret);
					}else{
						printf("Connected on port %d from %s\n", client.sin_port, inet_ntoa(client.sin_addr));
						memset (buff, 0, SERVE_BUFFER_SIZE);
						while(1){
							i++;
							if(!(i % 60)){
								buff[0] = i;
								net_send(csock, buff, SERVE_BUFFER_SIZE, 0);
							}
							VIDEO_WaitVSync();
						}
						net_close(csock);
					}
				}
			}
		}
	}
	return NULL;
}
