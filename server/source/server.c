#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <ogcsys.h>
#include <gccore.h>
#include <network.h>
#include <ogc/pad.h>
#include <ogc/es.h>
#include <ogc/machine/processor.h>
#include <ogc/ipc.h>
#include <ogc/wiilaunch.h>

#define SERVE_PORT 8034
#define SERVE_BUFFER_SIZE 1024

s32 launch_HBC();
void *initialize();
void *serve (void *arg);
s32 WII_LaunchTitleBackground(u64 titleID);
static s32 __WII_WriteNANDBootInfo(void);
static u32 __CalcChecksum(u32 *buf, int len);
static void __SetChecksum(void *buf, int len);
s32 InitializeWii(void);
static s32 __WII_WriteNANDBootInfo(void);
static s32 __WII_ReadNANDBootInfo(void);
static s32 __WII_ReadStateFlags(void);
static int __ValidChecksum(void *buf, int len);


typedef struct {
	u32 checksum;
	u32 argsoff;
	u8 unk1;
	u8 unk2;
	u8 apptype;
	u8 titletype;
	u32 launchcode;
	u32 unknown[2];
	u64 launcher;
	u8 argbuf[0x1000];
} NANDBootInfo;

typedef struct {
	u32 checksum;
	u8 flags;
	u8 type;
	u8 discstate;
	u8 returnto;
	u32 unknown[6];
} StateFlags;

static char __nandbootinfo[] ATTRIBUTE_ALIGN(32) = "/shared2/sys/NANDBOOTINFO";
static char __stateflags[] ATTRIBUTE_ALIGN(32) = "/title/00000001/00000002/data/state.dat";

static NANDBootInfo nandboot ATTRIBUTE_ALIGN(32);
static StateFlags stateflags ATTRIBUTE_ALIGN(32);

static int __initialized = 0;
static char args_set = 0;


static void *framebuffer = NULL;
static GXRModeObj *rmode = NULL;

static	lwp_t server_handle = (lwp_t)NULL;



//---------------------------------------------------------------------------------
int main(int argc, char **argv) {
//---------------------------------------------------------------------------------

	s32 ret;

	int i=0;
	char localip[16] = {0};
	char gateway[16] = {0};
	char netmask[16] = {0};
	u32 pressedPort[4];

	framebuffer = initialize();

	// The console understands VT terminal escape codes
	// This positions the cursor on row 2, column 0
	// we can use variables for this with format codes too
	// e.g. printf ("\x1b[%d;%dH", row, column );
	printf("\x1b[2;0H");

	printf("Configuring network ...\n");

	ret = if_config(localip, netmask, gateway, TRUE, 20);
	if(ret >= 0){
		printf("Network configured:\n"
			"\tIP:%s\n\tGATEWAY:\t%s\n\tMASK:\t%s\n", localip, gateway, netmask);
		LWP_CreateThread(&server_handle,	/* thread handle */
							serve,			/* code */
							localip,		/* arg pointer for thread */
							NULL,			/* stack base */
							16*1024,		/* stack size */
							50				/* thread priority */ );
	}else{
		printf("Could not configure the network. Are you online?\n");
	}

	printf("Network ready. Press Start on your GameCube controller to launch Homebrew Channel.\n");


	while(1) {

		// Call WPAD_ScanPads each loop, this reads the latest controller states
		PAD_ScanPads();

		// WPAD_ButtonsDown tells us which buttons were pressed in this loop
		// this is a "one shot" state which will not fire again until the button has been released
		for(i=0; i<4; i++){
			pressedPort[i] = PAD_ButtonsDown(i);
			if(pressedPort[i] & PAD_BUTTON_START){
				printf("Return code for HBC launch: %d\n", launch_HBC());
				//WII_LaunchTitle(0x100014C554C5ALL); // This title ID is LULZ, the Homebrew Channel's
			}
		}			

		// Wait for the next frame
		VIDEO_WaitVSync();
	}

	return 0;
}

s32 launch_HBC(){
	return WII_LaunchTitleBackground(0x100014C554C5ALL); // This title ID is LULZ, the Homebrew Channel's
}

//---------------------------------------------------------------------------------
void *initialize() {
//---------------------------------------------------------------------------------

	void *framebuffer;

	InitializeWii();
	VIDEO_Init();
	PAD_Init();

	rmode = VIDEO_GetPreferredMode(NULL);
	framebuffer = MEM_K0_TO_K1(SYS_AllocateFramebuffer(rmode));
	console_init(framebuffer,20,20,rmode->fbWidth,rmode->xfbHeight,rmode->fbWidth*VI_DISPLAY_PIX_SZ);

	VIDEO_Configure(rmode);
	VIDEO_SetNextFramebuffer(framebuffer);
	VIDEO_SetBlack(FALSE);
	VIDEO_Flush();
	VIDEO_WaitVSync();
	if(rmode->viTVMode&VI_NON_INTERLACE) VIDEO_WaitVSync();

	return framebuffer;

}

void *serve (void *arg){

	int i = 0;
	int sock, csock;
	int ret;
	u32 clientlen;
	struct sockaddr_in client;
	struct sockaddr_in server;
	char buff[SERVE_BUFFER_SIZE];

	clientlen = sizeof(client);

	sock = net_socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
	if (sock == INVALID_SOCKET){
		printf("Couldn't create a socket.\n");
	}else{
		memset (&server, 0, sizeof (server));
		memset (&client, 0, sizeof (client));

		server.sin_family = AF_INET;
		server.sin_port = htons (SERVE_PORT);
		server.sin_addr.s_addr = INADDR_ANY;
		ret = net_bind(sock, (struct sockaddr *)&server, sizeof(server));

		if(ret){
			printf("Error %d when trying to bind socket to port %d.\n", ret, SERVE_PORT);
		}else{
			if((ret = net_listen(sock, 1))){
				printf("Error %d listening to socket.\n", ret);
			}else{
				while(1){
					csock = net_accept(sock, (struct sockaddr*)&client, &clientlen);
					if (csock < 0){
						printf("Couldn't accept incoming clients. (Error %d)\n", ret);
					}else{
						printf("Connected on port %d from %s\n", client.sin_port, inet_ntoa(client.sin_addr));
						memset (buff, 0, SERVE_BUFFER_SIZE);
						while(1){
							i++;
							if(!(i % 60)){
								buff[0] = i;
								net_send(csock, buff, SERVE_BUFFER_SIZE, 0);
							}
							VIDEO_WaitVSync();
						}
						net_close(csock);
					}
				}
			}
		}
	}
	return NULL;
}

s32 WII_LaunchTitleBackground(u64 titleID){
	s32 res;
	u32 numviews;
	STACK_ALIGN(tikview,views,4,32);

	if(!__initialized)
		return WII_ENOTINIT;

	res = ES_GetNumTicketViews(titleID, &numviews);
	if(res < 0) {
		return res;
	}
	if(numviews > 4) {
		return WII_EINTERNAL;
	}
	res = ES_GetTicketViews(titleID, views, numviews);
	if(res < 0)
		return res;

	net_wc24cleanup();

	if (args_set == 0)
	{
		memset(&nandboot,0,sizeof(NANDBootInfo));
		nandboot.apptype = 0x81;
		if(titleID == 0x100000002LL)
			nandboot.titletype = 4;
		else
			nandboot.titletype = 2;
		if(ES_GetTitleID(&nandboot.launcher) < 0)
			nandboot.launcher = 0x0000000100000002LL;
		nandboot.checksum = __CalcChecksum((u32*)&nandboot,sizeof(NANDBootInfo));
		__WII_WriteNANDBootInfo();
	}
	VIDEO_SetBlack(1);
	VIDEO_Flush();

	res = ES_LaunchTitleBackground(titleID, &views[0]);
	if(res < 0)
		return res;
	return WII_EINTERNAL;
}

static s32 __WII_WriteNANDBootInfo(void){
	int fd;
	int ret;

	__SetChecksum(&nandboot, sizeof(nandboot));

	fd = IOS_Open(__nandbootinfo,IPC_OPEN_READ|IPC_OPEN_WRITE);
	if(fd < 0) {
		return WII_EINTERNAL;
	}

	ret = IOS_Write(fd, &nandboot, sizeof(nandboot));
	IOS_Close(fd);
	if(ret != sizeof(nandboot)) {
		return WII_EINTERNAL;
	}
	return 0;
}

static u32 __CalcChecksum(u32 *buf, int len){
	u32 sum = 0;
	int i;
	len = (len/4);

	for(i=1; i<len; i++)
		sum += buf[i];

	return sum;
}

static void __SetChecksum(void *buf, int len){
	u32 *p = (u32*)buf;
	p[0] = __CalcChecksum(p, len);
}

s32 InitializeWii(void){
	__WII_ReadStateFlags();
	__WII_ReadNANDBootInfo();
	__initialized = 1;
	return 0;
}

static s32 __WII_ReadNANDBootInfo(void){
	int fd;
	int ret;

	fd = IOS_Open(__nandbootinfo,IPC_OPEN_READ);
	if(fd < 0) {
		memset(&nandboot,0,sizeof(nandboot));
		return WII_EINTERNAL;
	}

	ret = IOS_Read(fd, &nandboot, sizeof(nandboot));
	IOS_Close(fd);
	if(ret != sizeof(nandboot)) {
		memset(&nandboot,0,sizeof(nandboot));
		return WII_EINTERNAL;
	}
	if(!__ValidChecksum(&nandboot, sizeof(nandboot))) {
		memset(&nandboot,0,sizeof(nandboot));
		return WII_ECHECKSUM;
	}
	return 0;
}

static s32 __WII_ReadStateFlags(void){
	int fd;
	int ret;

	fd = IOS_Open(__stateflags,IPC_OPEN_READ);
	if(fd < 0) {
		memset(&stateflags,0,sizeof(stateflags));
		return WII_EINTERNAL;
	}

	ret = IOS_Read(fd, &stateflags, sizeof(stateflags));
	IOS_Close(fd);
	if(ret != sizeof(stateflags)) {
		memset(&stateflags,0,sizeof(stateflags));
		return WII_EINTERNAL;
	}
	if(!__ValidChecksum(&stateflags, sizeof(stateflags))) {
		memset(&stateflags,0,sizeof(stateflags));
		return WII_ECHECKSUM;
	}
	return 0;
}

static int __ValidChecksum(void *buf, int len){
	u32 *p = (u32*)buf;
	return p[0] == __CalcChecksum(p, len);
}
